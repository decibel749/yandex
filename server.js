const express = require('express');
const app = express();
const fs = require("fs");

app.engine('.html', require('ejs').__express);
app.set('views', __dirname + '\\views');
app.set('view engine', 'ejs');

function getContentByParams(params) {
    return JSON.parse(params);
}

app.get('/search/:id', (req, res) => {
    const myid = req.params.id;
    if (myid != 'main.css' &&
        myid != 'main.js' &&
        myid != 'jquery-3.3.1.min.js') {
        const json = fs.readFileSync("./data.json");
        let body = getContentByParams(json);
        var all = Object.keys(body).length;
        // console.log(myid, all);
        if (myid > all) {
            res.redirect('/404');
            return;
        }
        for (let i = 1; i < all + 1; i++) {
            if (i != myid) delete body[i];
        }
        res.render('search', {
            data: body,
            id: myid
        });
    } else
        res.redirect('/');
});

app.get('/arrival', (req, res) => {
    const json = fs.readFileSync("./data.json");
    let body = getContentByParams(json);
    res.render('arrival', {
        data: body
    });
});

app.get('/delay', (req, res) => {
    const json = fs.readFileSync("./data.json");
    var body = getContentByParams(json);
    console.log('/delay');
    res.render('delay', {
        data: body
    });
});

app.get('/departure', (req, res) => {
    const json = fs.readFileSync("./data.json");
    var body = getContentByParams(json);
    console.log('/departure');
    res.render('departure', {
        data: body
    });
});

app.get('/', (req, res) => {
    const json = fs.readFileSync("./data.json");
    var body = getContentByParams(json);
    // console.log('/');
    res.render('home', {
        data: body
    });
});

app.use(function (req, res, next) {
    // console.log('Time:', Date.now(),req.url);
    res.send('404');
    next();
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});