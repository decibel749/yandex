function Ticker(id) {
    this.id = id;
    this._i = 0;
    inc_i = () => {
        console.log(this.id,this._i++);
        return this._i; 
    }
}

Ticker.prototype = {
    constructor : Ticker,
    tick : function () {
        inc_i();
    }
}

var ticker  = new Ticker('#1');
var ticker2 = new Ticker('#0');

// ticker.tick();
// ticker.tick();
// ticker.tick();

// ticker2.tick();
// ticker2.tick();
// ticker2.tick();
// ticker2.tick();


var time = setInterval(ticker.tick,1000);
setTimeout(() => {
    clearInterval(time);
    console.log('Остановка 1');
    time2 = setInterval(ticker2.tick,1000);
    setTimeout(()=> {
        clearInterval(time2);
        console.log('Остановка 2');
    },4000)
},4000);
